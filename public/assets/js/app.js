// On appelle les elements utiles

const pug = document.querySelector('#pug');
const roc = document.querySelector('#roc');
const score = document.querySelector('#score');

// on définit la fonction jump pour définir se qui
// se passe quand le pug saute
function jump() {
    pug.classList.add('jump-pug');
    // on retire l'action jump settimeout pour un delai d'attente
    setTimeout(() => {
        pug.classList.remove('jump-pug');
    },500);
}

// on n'utilise la fonction des que l'utilisateur 
// appuie sur la touche espace
// nous allons donc écouter les touches

document.addEventListener('keypress', () => {
    // saute que si le pug n'esty pas en mvt    
    jump();
    console.log('cc');

})

// pour avoir la valeur en hauteur du pug
// pour savoir ou se situe la crotte de licorne
setInterval(() => {
    score.innerText++;
    const pugTop = parseInt(window.getComputedStyle(pug).getPropertyValue('top'));
    const rocLeft = parseInt(window.getComputedStyle(roc).getPropertyValue('left'));
    console.log(rocLeft);
    if (rocLeft < 0) {
        roc.style.display = 'none';
    } else {
        roc.style.display = '';
    }
    if (rocLeft < 50 && rocLeft > 0 && pugTop > 300) {
        alert('GAME OVER 😨. votre score est de :' + score.innerText +"\n\nRejouer ?")
    location.reload();
    }
}, 50)